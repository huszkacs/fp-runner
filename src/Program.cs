﻿using System;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace FPRunner
{
    class Program
    {
        static void Main(string[] args) {
            CreateHostBuilder(args).Build().Run();
        }

        static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
            .ConfigureServices(ConfigureServices);

        static void ConfigureServices(IServiceCollection services) {
            services.AddHostedService<Runner>();
        }
    }
}
